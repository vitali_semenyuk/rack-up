#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require 'prawn'
require 'active_support'
require 'active_support/core_ext/date/calculations'
require 'active_support/core_ext/time/calculations'

AUTHOR = 'Vitali Semenyuk'
PROJECT_DIRECTORIES = %w[calendly].freeze
LINES_LIMIT = 1000

def get_commits(date:, author:)
  from = date.prev_month.end_of_month.to_s
  to = date.next_month.beginning_of_month.to_s

  <<~SHELL
    git log --author='#{author}' --since='#{from}' --until='#{to}' --no-merges --format=format:%H master
  SHELL
end

def get_changes(commit)
  `git show #{commit} -- '*.rb' '*.js' ':(exclude)*_spec.rb'`.lines&.[](6..)
end

if ARGV.size != 1
  warn('Please specify month (e.g. 10/2020)')
  exit 1
end

begin
  date = Date.strptime(ARGV.first, '%m/%Y')
rescue Date::Error
  warn('Please specify month (e.g. 10/2020)')
  exit 1
end

get_commits = get_commits(date:, author: AUTHOR)

result = []

PROJECT_DIRECTORIES.each do |dir|
  Dir.chdir("../#{dir}") do
    `#{get_commits}`.lines.map(&:strip).each do |commit|
      changes = get_changes(commit)
      result.concat(changes) if changes
      break if result.size > LINES_LIMIT
    end
  end
end

filename = "RackUp-#{date}"
text_filename = "out/#{filename}.txt"
pdf_filename = "out/#{filename}.pdf"
system('mkdir -p out')

File.write(text_filename, result.join)

Prawn::Document.generate(pdf_filename) do
  font_families.update('JetBrainsMono' => {
                         normal: 'assets/JetBrainsMono-Regular.ttf',
                         italic: 'assets/JetBrainsMono-Italic.ttf'
                       })
  font 'JetBrainsMono'
  text result.join("\n")
end
